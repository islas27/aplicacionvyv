<!DOCTYPE html>
<?php
  function randomColor() {
    return "hsl(".rand(0,359).",100%,50%)";
  }
  include("resources/connection.php");
  if(!$conexion = connection()){
    $_GET['error']=1;
  }else{
    $query = 'select concat_ws(" ",nombre,apellidoPat) as nombre, pesoKilos as peso, estaturaMetros as estatura from personas;';
    if(!$result = mysqli_query($conexion,$query)){
      $_GET['error']=1;
    }else{
      while($row = $result->fetch_assoc()){
        $nombres[] = $row['nombre'];
        $alturas[] = $row['estatura'];
        $array['value'] = (int) $row['peso'];
        $array['color'] = randomColor();
        $array['label'] = $row['nombre'];
        $superArray[] = $array;
      }
    }
  }
?>
<html lang="en">
  <head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jonathan Islas</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/bootstrapValidator.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php include("resources/navbar.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
      <h2 style="margin-bottom: 24px">Graficaci�n</h2>
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
      <?php
        //Aqui poner mensaje de error=1||error=2
      ?>
      <!--Asi como usamos col para el espacio horizontal, tenemos row para el espacio vertical-->
      <div class="row" style="padding-left: 15px; padding-top: 15px">
          <p>Aqu� podr� realizar registros as� como observar las gr�ficas de los datos de estatura y peso.<br><br></p>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <form  class="form-horizontal" id="FRMregistro" name="FRMregistro" role="form" action="control/personaCTL.php" method="post">
          <!--En acuerdo con la documentacio de bootstrap, los labels e inputs se ponen dentro de un-->
          <!--contenedor con la clase "form-group"-->
          <legend><span class="glyphicon glyphicon-user" style="color: #0C84E4;"></span>&nbsp; &nbsp;Datos Personales</legend>
          <?php
            //Aqui poner mensaje de success, o error=4
          ?>
          <p>Ingrese su informaci�n, que ser� utilizada con fines estad�sticos unicamente.</p>
          <div class="form-group">
            <label class="col-md-4 control-label" for="nombre">Nombre</label>
            <div class="col-md-8">
              <input type="text" placeholder="Nombre" class="form-control input-sm" id="nombre" name="nombre"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="ap_paterno">Apellido paterno</label>
            <div class="col-md-8">
              <input type="text" placeholder="Apellido paterno" class="form-control input-sm" id="apellidoPat" name="apellidoPat"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="ap_materno">Apellido materno</label>
            <div class="col-md-8">
              <input type="text" placeholder="Apellido materno" class="form-control input-sm" id="apellidoMat" name="apellidoMat"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="pesoKilos">Peso (En Kg)</label>
            <div class="col-md-8">
              <input type="text" placeholder="Peso (En Kg)" class="form-control input-sm" id="pesoKilos" name="pesoKilos"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="estaturaMetros">Estatura (En m)</label>
            <div class="col-md-8">
              <input type="text" placeholder="Estatura (En m)" class="form-control input-sm" id="estaturaMetros" name="estaturaMetros"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">G�nero</label>
            <div class="col-md-8">
              <div class="radio">
                <label><input name="genero" id="optionsRadios1" value="m" checked="" type="radio">Masculino</label>
              </div>
              <div class="radio">
                <label><input name="genero" id="optionsRadios2" value="f" type="radio">Femenino</label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="fechaNacimiento">Fecha Nacimiento</label>
            <div class="col-md-8">
              <input type="text" placeholder="YYYY-MM-DD" class="form-control input-sm" id="fechaNacimiento" name="fechaNacimiento"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="rfc">RFC</label>
            <div class="col-md-8">
              <input type="text" placeholder="RFC" class="form-control input-sm" id="rfc" name="rfc"/>
            </div>
          </div>
          <div class="form-group" style="padding-top: 25px; padding-left: 25px;">
            <input type="button" class="btn btn-primary col-sm-3 col-md-offset-4" style="margin-right: 15px;" onclick="borrarForma()" value="Borrar Forma"/>
            <button name="cmdAction" type="submit" class="btn btn-primary col-sm-3" value="0">Agregar</button>
          </div>
          </form>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <legend><span class="glyphicon glyphicon-stats" style="color: #0C84E4"></span>&nbsp; &nbsp;Gr�ficas</legend>
          <p>Aqu� podr� ver las gr�ficas generadas con los datos almacenados.</p>
          <canvas id="graficaAltura" class="col-sm-12"></canvas>
          <canvas id="graficaPeso" class="col-sm-12"></canvas>

        </div>
      </div>

      <br>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('#fechaNacimiento').datepicker({ dateFormat: "yy-mm-dd" });
      
      function borrarForma() {
        document.getElementById("FRMregistro").reset();
      }
    </script>
    <script type="text/javascript" src="js/bootstrapValidator.js"></script>
    <script type="text/javascript" src="js/validator/validadorRegistro.js"></script>
    <script src="js/Chart.js"></script>
    <script type="text/javascript">
      var nom = <?php echo json_encode($nombres) ?>;
      var est = <?php echo json_encode($alturas) ?>;
      var doughnutData = <?php echo json_encode($superArray) ?>;
    </script>
    <script src="resources/graficaAltura.js"></script>

  </body>
</html>