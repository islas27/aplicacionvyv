<!DOCTYPE html>
<?php
    function randomColor() {

        return "hsl(".rand(0,359).",100%,50%)";
    }

    include("/resources/connection.php");
    $conexion = connection();
    $query = 'select concat_ws(" ",nombre,apellidoPat) as nombre, pesoKilos as peso, estaturaMetros as estatura from personas;';
    $result = mysqli_query($conexion,$query);
    while($row = $result->fetch_assoc()){
        $array['value'] = $row['peso'];
        $array['color'] = randomColor();
        $array['label'] = $row['nombre'];
        $superArray[] = $array;
    }
    ECHO json_encode($superArray, JSON_PRETTY_PRINT);
?>
