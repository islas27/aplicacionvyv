$(document).ready(function() {
    $('#FRMregistro').bootstrapValidator({
        message: 'Este valor no es aceptable',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nombre: {
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vac�o'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z�������������� ]+$/,
                        message: 'No se permiten los caracteres especiales'
                    }
                }
            },
            apellidoPat: {
                validators: {
                    notEmpty: {
                        message: 'El apellido no puede estar vac�o'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z�������������� ]+$/,
                        message: 'No se permiten los caracteres especiales'
                    }
                }
            },
            apellidoMat: {
                validators: {
                    notEmpty: {
                        message: 'El apellido no puede estar vac�o'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z�������������� ]+$/,
                        message: 'No se permiten los caracteres especiales'
                    }
                }
            },
            pesoKilos: {
                validators: {
                    notEmpty: {
                        message: 'No puede estar vac�o este campo'
                    },
                    between: {
                        min: 1,
                        max: 400,
                        message: 'Debe de usar valores positivos y menores a 400kgs'
                    }
                }
            },
            estaturaMetros: {
                validators: {
                    notEmpty: {
                        message: 'No puede estar vac�o este campo'
                    },
                    between: {
                        min: 0.25,
                        max: 2.90,
                        message: 'Debe de usar valores mayores a 0.25 menores a 2.90mts'
                    }
                }
            },
            genero: {
                validators: {
                    notEmpty: {
                        message: 'El genero es requerido'
                    }
                }
            },
            fechaNacimiento: {
                validators: {
                    notEmpty: {
                        message: 'La fecha de nacimiento es requerida'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'No es una fecha valida'
                    }
                }
            },
            rfc: {
                validators: {
                    notEmpty: {
                        message: 'El RFC no puede estar vac�o'
                    },
                    regexp: {
                        regexp: /^[A-Z0-9]+$/,
                        message: 'No se permiten los caracteres especiales, ni letras minusculas'
                    }
                }
            }
        }
    });
});