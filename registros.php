<!DOCTYPE html>
<?php
  include("resources/connection.php");
  if(!$conexion = connection()){
    header("Location: index.php?error=1");
  }else{
    $query = 'select idPersona, concat_ws(" ",nombre,apellidoPat,apellidoMat) as nombre, pesoKilos, estaturaMetros,
                vigente, rfc, genero, fechaNacimiento, fechaRegistro from personas;';
    if(!$result = mysqli_query($conexion,$query)){
      header("Location: index.php?error=1");
      exit();
    }
  }
?>
<html lang="en">
  <head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jonathan Islas</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/footable.core.min.css" rel="stylesheet"/>
    <link href="css/bootstrapValidator.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php include("resources/navbar.php");?>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
      <h2 style="margin-bottom: 24px">Graficaci�n: Registros Almancenados</h2>
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 contentPanel">
      <?php
        //Aqui poner errores y mensajes globales
      ?>
      <!--Asi como usamos col para el espacio horizontal, tenemos row para el espacio vertical-->
      <div class="row" style="padding-left: 15px; padding-top: 15px">
          <p>En esta pagina puede ver la base de datos, asi como editar los registros existentes.<br><br></p><!--Instrucciones-->
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <legend><span class="glyphicon glyphicon-list" style="color: #0C84E4"></span>&nbsp; &nbsp;Registros Almacenados</legend>
          <p>A continuacion puede encontrar todos los registros almacenados en la base de datos, asi como un peque�o menu de opciones</p>
          <table class="footable table table-striped table-hover table-condensed toggle-arrow-circle" data-page-size="15">
            <thead>
              <tr>
                <th data-type="numeric" data-sort-initial="true">ID</th>
                <th data-toggle="true">Nombre Completo</th>
                <th data-hide="phone" data-type="numeric">Peso(Kg)</th>
                <th data-hide="phone" data-type="numeric">Estatura(m)</th>
                <th data-hide="all">Vigencia</th>
                <th data-hide="all">RFC</th>
                <th data-hide="all">G�nero</th>
                <th data-hide="all">Fecha Nacimiento</th>
                <th data-hide="all">Fecha Registro</th>
                <th data-hide="all">Edici�n</th>
              </tr>
            </thead>
            <tbody>
              <?php
                /* fetch associative array */
                while ($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo "<td>".$row['idPersona']."</td>";
                    echo "<td>".$row['nombre']."</td>";
                    echo "<td>".$row['pesoKilos']."</td>";
                    echo "<td>".$row['estaturaMetros']."</td>";
                    echo ($row['vigente'] == 1)? '<td><span class="glyphicon glyphicon-ok" style="color: #0C84E4"></span></td>'
                          : '<td><span class="glyphicon glyphicon-remove" style="color: red"></span></td>' ;
                    echo "<td>".$row['rfc']."</td>";
                    echo "<td>".$row['genero']."</td>";
                    echo "<td>".$row['fechaNacimiento']."</td>";
                    echo "<td>".$row['fechaRegistro']."</td>";
                    echo '<td><a href="registros.php?idPersona='.$row['idPersona'].'" class="btn btn-xs btn-info">Editar</a></td>';
                    echo '</tr>';
                }
                /* free result set */
                $result->free();
              ?>
            </tbody>
            <tfoot>
              <tr>
                  <td colspan="5" class="text-center col-xs-6">
                      <ul class="pagination pagination-centered"></ul>
                  </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <legend><span class="glyphicon glyphicon-pencil" style="color: #0C84E4"></span>&nbsp; &nbsp;Edici�n de registros</legend>
          <p>Si ha seleccionado un registro a editar, este se cargara en esta secci�n</p><br>
         
          <?php
            if(isset($_GET['idPersona'])){
              $idPersona = $_GET['idPersona'];
              $query = "select * from personas where idPersona = $idPersona";
              if(!$result = mysqli_query($conexion,$query)){
                header("Location: index.php?error=1");
                exit();
              }else{
                if(!$row = $result->fetch_assoc()){
                  $d = 'disabled';
                }else{
                  $d = '';
                }
              }
            }else{
              $d = 'disabled';
            }
          ?>
          <form  class="form-horizontal" id="FRMregistro" name="FRMregistro" role="form" action="control/personaCTL.php" method="post">
          <div class="form-group">
            <label class="col-md-4 control-label" for="nombre">Nombre</label>
            <div class="col-md-8">
              <input type="text" placeholder="Nombre" class="form-control input-sm" id="nombre" name="nombre" <?php echo $d?> value="<?php echo$row['nombre']?>"/>
              <input type="text" hidden id="idPersona" name="idPersona" <?php echo $d?> value="<?php echo $row['idPersona']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="ap_paterno">Apellido paterno</label>
            <div class="col-md-8">
              <input type="text" placeholder="Apellido paterno" class="form-control input-sm" id="apellidoPat" name="apellidoPat" <?php echo $d?> value="<?php echo$row['apellidoPat']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="ap_materno">Apellido materno</label>
            <div class="col-md-8">
              <input type="text" placeholder="Apellido materno" class="form-control input-sm" id="apellidoMat" name="apellidoMat" <?php echo $d?> value="<?php echo$row['apellidoMat']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="pesoKilos">Peso (En Kg)</label>
            <div class="col-md-8">
              <input type="text" placeholder="Peso (En Kg)" class="form-control input-sm" id="pesoKilos" name="pesoKilos" <?php echo $d?> value="<?php echo $row['pesoKilos']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="estaturaMetros">Estatura (En m)</label>
            <div class="col-md-8">
              <input type="text" placeholder="Estatura (En m)" class="form-control input-sm" id="estaturaMetros" name="estaturaMetros" <?php echo $d?> value="<?php echo $row['estaturaMetros']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">G�nero</label>
            <div class="col-md-8">
              <div class="radio">
                <?php $c = ($row['genero'] == 'm')? 'checked': '';?>
                <label><input name="genero" id="optionsRadios1" value="m" type="radio" <?php echo $d." ".$c;?>>Masculino</label>
              </div>
              <div class="radio">
                <?php $c = ($row['genero'] == 'f')? 'checked': '';?>
                <label><input name="genero" id="optionsRadios2" value="f" type="radio" <?php echo $d." ".$c;?>>Femenino</label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="fechaNacimiento">Fecha Nacimiento</label>
            <div class="col-md-8">
              <input type="text" placeholder="YYYY-MM-DD" class="form-control input-sm" id="fechaNacimiento" name="fechaNacimiento" <?php echo $d?> value="<?php echo$row['fechaNacimiento']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="rfc">RFC</label>
            <div class="col-md-8">
              <input type="text" placeholder="RFC" class="form-control input-sm" id="rfc" name="rfc" <?php echo $d?> value="<?php echo$row['rfc']?>"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="vigencia">Vigencia</label>
            <div class="col-xs-2">
              <?php $c = ($row['vigente'] == 1)? 'checked': '';?>
              <input type="checkbox"  placeholder="RFC" class="form-control input-sm" id="vigencia" name="vigencia" <?php echo $d." ".$c ;?>/>
            </div>
          </div>
          <div class="form-group" style="padding-top: 25px; padding-left: 25px;">
            <button name="cmdAction" type="submit" style="margin-right: 15px;" class="btn btn-warning col-sm-3 col-md-offset-4"value="1" <?php echo $d?>>Editar</button>
            <a href="control/personaCTL.php?cmdAction=2&idPersona=<?php echo $row['idPersona']?>" <?php echo $d?> class="btn btn-danger col-sm-3">Eliminar</a>
            <!--<button name="cmdAction" type="submit" class="btn btn-danger col-sm-3"  onclick="borrarValidador()"  value="2" <?php echo $d?>>Eliminar</button>-->
          </div>
          </form>
        </div>
      </div>
      <div class="row">
        
      </div>
      <br>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/footable.min.js"></script>
    <script src="js/footable.paginate.min.js"></script>
    <script src="js/footable.sort.min.js"></script>
    <script>
      $('#fechaNacimiento').datepicker({ dateFormat: "yy-mm-dd" });
      var bootstrapValidator = $('#FRMRegistro').data('bootstrapValidator');
      function borrarValidador(){
        bootstrapValidator.destroy();
      }
    </script>
    <script type="text/javascript">$(function(){$('.footable').footable();});</script>
    <script type="text/javascript" src="js/bootstrapValidator.js"></script>
    <script type="text/javascript" src="js/validator/validadorRegistro.js"></script>
  </body>
</html>