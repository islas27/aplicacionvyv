# Aplicación Graficadora para Verificación y validación 2014-B #

## Descripción ##

Esta es una aplicación diseñada para graficar los datos de alumnos encuestados con su estatura y peso.
Esta diseñada en PHP, usando bootstrap como framework para la interfaz gráfica.
Se utilizara un plugin en php para la graficación.

### Instalación ###

Poner el repositorio completo en la carpeta de trabajo del servidor (www para el caso de WAMP), y entrar a ella desde http://localhost

### Contribuciones###

Diseñada, revisada y publicada por Jonathan Islas