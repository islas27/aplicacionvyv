var barChartData = {
    labels: nom,
    datasets: [
        {
            label: "Estaturas",
            fillColor: "rgba(12,132,228,0.5)",
            strokeColor: "rgba(12,132,228,0.8)",
            highlightFill: "rgba(12,132,228,0.75)",
            highlightStroke: "rgba(12,132,228,1)",
            data: est
        }
    ]
}

window.onload = function(){
		var ctxtwo = document.getElementById("graficaPeso").getContext("2d");
		window.myDoughnut = new Chart(ctxtwo).Doughnut(doughnutData, {
			responsive : true
		});
                
                var ctx = document.getElementById("graficaAltura").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
}
