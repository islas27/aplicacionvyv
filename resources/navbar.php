<div class="navbar navbar-inverse">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="http://mx.linkedin.com/pub/jonathan-islas/99/b40/333/">Jonathan Islas</a>
  </div>
  <div class="navbar-collapse collapse navbar-inverse-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle active" data-toggle="dropdown">Graficación <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="index.php">Inicio</a></li>
          <li><a href="registros.php">Listas</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle active" data-toggle="dropdown">Recursos <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="http://getbootstrap.com/"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Bootstrap</a></li>
          <li><a href="http://bootswatch.com/"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Bootswatch</a></li>
          <li><a href="http://bootstrapvalidator.com/"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Bootstrap Validator</a></li>
          <li><a href="http://fooplugins.com/plugins/footable-jquery/"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> FooTable</a></li>
          <li><a href="http://www.chartjs.org/"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Chart.js</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>