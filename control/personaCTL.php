<?php
include("../resources/connection.php");
    if(isset($_POST['nombre']) || isset($_GET['idPersona'])){
        if($conexion = connection()){
            $option = (isset($_POST['cmdAction']))? $_POST['cmdAction']: $_GET['cmdAction'];
            switch($option){
                case 0://agregar datos
                    $persona = captura();
                    if ($query = $conexion->prepare("INSERT INTO personas(nombre,apellidoPat,apellidoMat,pesoKilos,estaturaMetros,genero,fechaNacimiento,fechaRegistro,rfc)
                                                    VALUES (?,?,?,?,?,?,?,now(),?);")) {
                        if($query ->bind_param("sssddsss", $persona[0],$persona[1],$persona[2],$persona[3],$persona[4],$persona[5],$persona[6],$persona[7])){
                            if ($query ->execute()) {
                                header("Location: ../index.php?success=1");//agregado con exito
                                exit();
                            }else{
                                header("Location: ../index.php?error=4");//error desconocido con la query o intento de injeccion....
                                exit();
                            }
                        }else{
                            header("Location: ../index.php?error=4");//error desconocido con la query o intento de injeccion....
                            exit();
                        }
                    }else{
                        header("Location: ../index.php?error=4");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    break;
                case 1://editar datos
                    $persona = captura();
                    if ($query = $conexion->prepare("update personas set nombre = ?,apellidoPat = ?,apellidoMat = ?,pesoKilos = ?,estaturaMetros = ?,
                                                    genero = ?,fechaNacimiento = ?,fechaRegistro = now(),rfc = ? where idPersona = ?;")) {
                        if($query ->bind_param("sssddsssi", $persona[0],$persona[1],$persona[2],$persona[3],$persona[4],$persona[5],$persona[6],$persona[7],$persona['id'])){
                            if ($query ->execute()) {
                                header("Location: ../registros.php?success=1");//editado con exito
                                exit();
                            }else{
                                header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                                exit();
                            }
                        }else{
                            header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                            exit();
                        }
                    }else{
                        header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    break;
                case 2://eliminar libro
                    $persona['id'] = $_GET['idPersona'];
                    if ($query = $conexion->prepare("delete from personas where idPersona = ?;")) {
                        if($query ->bind_param("i", $persona['id'])){
                            if ($query ->execute()) {
                                header("Location: ../registros.php?success=2");//editado con exito
                                exit();
                            }else{
                                header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                                exit();
                            }
                        }else{
                            header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                            exit();
                        }
                    }else{
                        header("Location: ../registros.php?error=4");//error desconocido con la query o intento de injeccion....
                        exit();
                    }
                    break;
            }
        }else{
            header("Location: ../index.php?error=1"); //conexion de BD cerrada...
            exit();
        }
    }else{
        header("Location: ../index.php?error=2"); //Se entro sin informacion relevante...
        exit();
    }
    
    function captura(){
        $persona[0] = $_POST['nombre'];
        $persona[1] = $_POST['apellidoPat'];
        $persona[2] = $_POST['apellidoMat'];
        $persona[3] = $_POST['pesoKilos'];
        $persona[4] = $_POST['estaturaMetros'];
        $persona[5] = $_POST['genero'];
        $persona[6] = $_POST['fechaNacimiento'];
        $persona[7] = $_POST['rfc'];
        if(isset($_POST['idPersona']))$persona['id'] = $_POST['idPersona'];
        return $persona;
    }

?>